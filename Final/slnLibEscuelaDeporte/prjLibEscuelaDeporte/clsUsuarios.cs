﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace libPersona
{
    public abstract class clsUsuarios
    {
        #region Atributos
       protected  string Nombre;
       protected  string Apellido;
       protected  string Documento;
       protected  string Celular;
       protected  string Usuario;
       protected  string Contrasena;
        protected string Correo;
        protected string Direccion;
        protected DateTime FechaNacimineto;
        #endregion

        #region Constructor
        public clsUsuarios()
        {
            this.Nombre = string.Empty;
            this.Apellido = string.Empty;
            this.Documento = string.Empty;
            this.Celular = string.Empty;
            this.Usuario = string.Empty;
            this.Contrasena = string.Empty;
            this.Correo = string.Empty;
            this.Direccion = string.Empty;
            this.FechaNacimineto = new DateTime();
        }
        #endregion

        #region Propiedades
        public string Nombres { get => Nombre; set => Nombre = value; }
        public string Apellidos { get => Apellido; set => Apellido = value; }
        public string Documentos { get => Documento; set => Documento = value; }
        public string Celulares { get => Celular; set => Celular = value; }
        public string Usuarios { get => Usuario; set => Usuario = value; }
        public string Direcciones { get => Direccion; set => Direccion = value; }
        public string Contrasenas { get => Contrasena; set => Contrasena = value; }
        public string Correos { get => Correo; set => Correo = value; }
        public DateTime FechaNacimientos { get => FechaNacimineto; set => FechaNacimineto = value; }

        #endregion

        #region Metodos Publicos
        public abstract bool validarSesion();
        public abstract bool actualizar();
        public abstract bool consultar();
        #endregion
    }
}
