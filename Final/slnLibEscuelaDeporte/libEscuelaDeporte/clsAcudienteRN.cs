﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using libConexionBd;
using libPersona;

namespace libEscuelaDeporte
{
    public class clsAcudienteRN : clsUsuarios
    {
        #region Constantes
        private const int CERO = 0;        
        private const string ACTUALIZAR = "actualizar";
        private const string VALIDARSESION = "validarsesion";
        #endregion

        #region Atributos
        private string strNombreApp;
        private string strError;
        private object vrUnico;
        private SqlParameter[] objDatosRegistro;
        private clsConexionBd objCnx;
        private DataSet dsDatos;
        private string strOcupacion;

        #endregion

        #region Constructor
        public clsAcudienteRN(string strNombreApp)
        {
            this.strNombreApp = strNombreApp;
            this.strError = string.Empty;
        }

        #endregion

        #region Propiedades
        public string Error { get => strError; }
        public string Ocupacion { get => strOcupacion; set => strOcupacion = value; }
        public object VrUnico { get => vrUnico; }
        public DataSet DsDatos { get => dsDatos; }
        #endregion

        #region Metodos Privados 
        private bool agregarParametros(string metodoOrgien)
        {
            try
            {
                objDatosRegistro = new SqlParameter[1];
                switch (metodoOrgien.ToLower())
                {                  
                    case VALIDARSESION:
                        
                        objDatosRegistro = new SqlParameter[2];
                        objDatosRegistro[0] = new SqlParameter("@usuario", Usuarios);
                        objDatosRegistro[1] = new SqlParameter("@contrasena", Contrasenas);
                        break;
                   
                    case ACTUALIZAR:
                        
                        objDatosRegistro = new SqlParameter[6];
                        objDatosRegistro[0] = new SqlParameter("@correo", Correos);
                        objDatosRegistro[1] = new SqlParameter("@direccion", Direcciones);
                        objDatosRegistro[2] = new SqlParameter("@celular", Celulares);
                        objDatosRegistro[3] = new SqlParameter("@ocupacion", strOcupacion);
                        objDatosRegistro[4] = new SqlParameter("@usuario", Usuarios);
                        objDatosRegistro[5] = new SqlParameter("@contrasena", Contrasenas);
                        break;


                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        #endregion

        #region Metodos Publicos
       
        public override bool validarSesion()
        {
            try
            {
                if (!agregarParametros(VALIDARSESION))
                {
                    return false;
                }
                objCnx = new clsConexionBd(strNombreApp);
                objCnx.SQL = "SP_ValidaSesionAcudiente";
                objCnx.ParametrosSQL = objDatosRegistro;
                if (!objCnx.consultarValorUnico(true, true))
                {
                    strError = objCnx.Error;
                    objCnx.cerrarCnx();
                    objCnx = null;
                    return false;
                }
                vrUnico = objCnx.ValorUnico;
                objCnx.cerrarCnx();
                objCnx = null;
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        public override bool actualizar()
        {
            try
            {
                if (!agregarParametros(ACTUALIZAR))
                {
                    return false;
                }
                objCnx = new clsConexionBd(strNombreApp);
                objCnx.SQL = "SP_actualizarAcudiente";
                objCnx.ParametrosSQL = objDatosRegistro;
                if (!objCnx.ejecutarSentencia(true, true))
                {
                    strError = objCnx.Error;
                    objCnx.cerrarCnx();
                    objCnx = null;
                    return false;
                }
                objCnx.cerrarCnx();
                objCnx = null;
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public override bool consultar()
        {
            try
            {
                objCnx = new clsConexionBd(strNombreApp);
                objCnx.SQL = "SP_ConsultarAcudiente";
                if (!objCnx.llenarDataSet(false, true))
                {
                    strError = objCnx.Error;
                    objCnx.cerrarCnx();
                    objCnx = null;
                    return false;
                }
                dsDatos = objCnx.DataSetLleno;
                objCnx.cerrarCnx();
                objCnx = null;
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
