﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using libLlenarCombos;
using libConexionBd;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;

namespace libEscuelaDeporte
{
    public class clsListaClaseRN
    {
        #region Constantes
        private const int MENOSUNO = -1;
        private const int CERO = 0;
        private const string REGISTRAR = "registrar";
        
        #endregion

        #region Atributos
        private int intIdGrupo;
        private string strDocumentoEstudiante;
        private string strError;
        private string strNombreApp;
        private SqlParameter[] objDatosRegistro;
        private clsConexionBd objCnx;
        

        #endregion

        #region Constructor

        public clsListaClaseRN( string strNombreApp)
        {
            this.intIdGrupo = MENOSUNO;
            this.strDocumentoEstudiante = string.Empty;
            this.strError = string.Empty;
            this.strNombreApp = strNombreApp;
        }
        #endregion

        #region Propiedades
        public int IdGrupo { set => intIdGrupo = value; }
        
        public string Documento {  set => strDocumentoEstudiante = value; }
        public string Error { get => strError; }
        
        
        #endregion

        #region Metodos Privados
        private bool Validar(string validar)
        {
            if (strNombreApp == string.Empty)
            {
                strError = "debe enviar el el nombre de la aplicacion";
                return false;
            }
            switch (validar)
            {
                case REGISTRAR:
                    if (strDocumentoEstudiante == string.Empty)
                    {
                        strError = "Debe ingresar un documento";
                        return false;
                    }
                    if (intIdGrupo <= CERO)
                    {
                        strError = "el id de grupo no puede ser inferior o igual que 0";
                        return false;
                    }
                    break;
                
            }
            return true;
        }
        private bool agregarParametros(string metodoOrigen)
        {
            try
            {
                if (!Validar(metodoOrigen))
                {
                    return false;
                }
                objDatosRegistro = new SqlParameter[1];
                switch (metodoOrigen.ToLower())
                {
                    case REGISTRAR:
                        objDatosRegistro = new SqlParameter[2];
                        objDatosRegistro[0] = new SqlParameter("@idGrupo", intIdGrupo);
                        objDatosRegistro[1] = new SqlParameter("@docEstudiante", strDocumentoEstudiante);
                        break;
                    
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Metodos Publicos
        public bool registrar()
        {
            try
            {
                if (!agregarParametros(REGISTRAR))
                {
                    return false;
                }
                objCnx = new clsConexionBd(strNombreApp);
                objCnx.SQL = "SP_RegistrarLista";
                objCnx.ParametrosSQL = objDatosRegistro;
                if (!objCnx.ejecutarSentencia(true, true))
                {
                    strError = objCnx.Error;
                    objCnx.cerrarCnx();
                    objCnx = null;
                    return false;
                }
                objCnx.cerrarCnx();
                objCnx = null;
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        #endregion

    }
}
