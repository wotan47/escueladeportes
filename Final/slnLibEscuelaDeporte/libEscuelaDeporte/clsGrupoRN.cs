﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using libLlenarCombos;
using libConexionBd;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;

namespace libEscuelaDeporte
{
    public class clsGrupoRN
    {
        #region Constantes
        private const int MENOSUNO = -1;
        private const int CERO = 0;
        private const string REGISTRAR = "registrar";
        private const string BORRAR = "borrar";
        private const string ACTUALIZAR = "actualizar"; 
        #endregion

        #region Atributos
        private int intIdGrupo;
        private int intIdHorario;
        private int intIdEscenario;
        private string strDeporte;
        private string strDocumento;
        private string strError;
        private string strNombreApp;
        private SqlParameter[] objDatosRegistro;
        private clsConexionBd objCnx;
        private DataSet dsDatos;

        #endregion

        #region Constructor

        public clsGrupoRN( string strNombreApp)
        {
            this.intIdHorario = MENOSUNO;
            this.intIdEscenario = MENOSUNO;
            this.strDeporte = string.Empty;
            this.strDocumento = string.Empty;
            this.strError = string.Empty;
            this.strNombreApp = strNombreApp;
        }
        #endregion

        #region Propiedades
        public int IdHorario { set => intIdHorario = value; }
        public int  IdEscenario {set => intIdEscenario = value; }
        public string Deporte {set => strDeporte = value; }
        public string Documento {  set => strDocumento = value; }
        public string Error { get => strError; }
        public int IdGrupo {  set => intIdGrupo = value; }
        public DataSet DsDatos { get => dsDatos; }
        #endregion

        #region Metodos Privados
        private bool Validar(string validar)
        {
            if (strNombreApp == string.Empty)
            {
                strError = "debe enviar el el nombre de la aplicacion";
                return false;
            }
            switch(validar)
            {
                case REGISTRAR:
                    if (strDocumento == string.Empty)
                    {
                    strError = "Debe ingresar un documento";
                    return false;
                    }
                    if (intIdEscenario <= CERO)
                    {
                    strError = "el id de escenario no puede ser inferior o igual que 0";
                    return false;
                    }
                    if (strDeporte == string.Empty)
                    {
                    strError = "el id de deporte no puede ser inferior o igual que 0";
                    return false;
                    }
                    if (intIdHorario <= CERO)
                    {
                    strError = "el id de horario no puede ser inferior o igual que 0";
                    return false;
                    }
                    break;
                case BORRAR:
                    if (intIdGrupo <= CERO)
                    {
                    strError = "el id de grupo no puede ser inferior o igual que 0";
                    return false;
                    }
                    break;
                case ACTUALIZAR:
                    if (strDocumento == string.Empty)
                    {
                    strError = "Debe ingresar un documento";
                    return false;
                    }
                    if (intIdEscenario <= CERO)
                    {
                    strError = "el id de escenario no puede ser inferior o igual que 0";
                    return false;
                    }
                    if (strDeporte == string.Empty)
                    {
                    strError = "el id de deporte no puede ser inferior o igual que 0";
                    return false;
                    }
                    if (intIdHorario <= CERO)
                    {
                    strError = "el id de horario no puede ser inferior o igual que 0";
                    return false;
                    }
                    if (intIdGrupo <= CERO)
                    {
                    strError = "el id de grupo no puede ser inferior o igual que 0";
                    return false;
                    }
                    break;
            }
            return true;
        }
        private bool agregarParametros(string metodoOrigen)
        {
            try
            {
                if (!Validar(metodoOrigen))
                {
                    return false;
                }
                objDatosRegistro = new SqlParameter[1];
                switch (metodoOrigen.ToLower())
                {
                    case REGISTRAR:
                        objDatosRegistro = new SqlParameter[4];
                        objDatosRegistro[0] = new SqlParameter("@Idhorario", intIdHorario);
                        objDatosRegistro[1] = new SqlParameter("@IdEscenario", intIdEscenario);
                        objDatosRegistro[2] = new SqlParameter("@Deporte", strDeporte);
                        objDatosRegistro[3] = new SqlParameter("@Documento", strDocumento);
                        break;
                    case BORRAR:
                        objDatosRegistro = new SqlParameter[1];
                        objDatosRegistro[0] = new SqlParameter("@IdGrupo", intIdGrupo);
                        break;
                    case ACTUALIZAR:
                        objDatosRegistro = new SqlParameter[5];
                        objDatosRegistro[0] = new SqlParameter("@Idhorario", intIdHorario);
                        objDatosRegistro[1] = new SqlParameter("@IdEscenario", intIdEscenario);
                        objDatosRegistro[2] = new SqlParameter("@Deporte", strDeporte);
                        objDatosRegistro[3] = new SqlParameter("@Documento", strDocumento);
                        objDatosRegistro[4] = new SqlParameter("@IdGrupo", intIdGrupo);
                        break;
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Metodos Publicos
        public bool registrar()
        {
            try
            {
                if (!agregarParametros(REGISTRAR))
                {
                    return false;
                }
                objCnx = new clsConexionBd(strNombreApp);
                objCnx.SQL = "SP_RegistrarGrupo";
                objCnx.ParametrosSQL = objDatosRegistro;
                if (!objCnx.ejecutarSentencia(true, true))
                {
                    strError = objCnx.Error;
                    objCnx.cerrarCnx();
                    objCnx = null;
                    return false;
                }
                objCnx.cerrarCnx();
                objCnx = null;
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool borrar()
        {
            try
            {
                if (!agregarParametros(BORRAR))
                {
                    return false;
                }
                objCnx = new clsConexionBd(strNombreApp);
                objCnx.SQL = "SP_eliminarGrupo";
                objCnx.ParametrosSQL = objDatosRegistro;
                if (!objCnx.ejecutarSentencia(true, true))
                {
                    strError = objCnx.Error;
                    objCnx.cerrarCnx();
                    objCnx = null;
                    return false;
                }
                objCnx.cerrarCnx();
                objCnx = null;
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool actualizar()
        {
            try
            {
                if (!agregarParametros(ACTUALIZAR))
                {
                    return false;
                }
                objCnx = new clsConexionBd(strNombreApp);
                objCnx.SQL = "SP_actualizarGrupo";
                objCnx.ParametrosSQL = objDatosRegistro;
                if (!objCnx.ejecutarSentencia(true, true))
                {
                    strError = objCnx.Error;
                    objCnx.cerrarCnx();
                    objCnx = null;
                    return false;
                }
                objCnx.cerrarCnx();
                objCnx = null;
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool consultar()
        {
            try
            {
                objCnx = new clsConexionBd(strNombreApp);
                objCnx.SQL = "SP_ConsultarGrupo";
                if (!objCnx.llenarDataSet(false, true))
                {
                    strError = objCnx.Error;
                    objCnx.cerrarCnx();
                    objCnx = null;
                    return false;
                }
                dsDatos = objCnx.DataSetLleno;
                objCnx.cerrarCnx();
                objCnx = null;
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

    }
}
