﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using libPersona;
using libConexionBd;
using System.Data;

namespace libEscuelaDeporte
{
    public class clsDirectorRN : clsUsuarios
    {
        #region Constantes
        private const int CERO = 0;
        private const string VALIDARSESION = "validarsesion";
        private const string REGISTRAR = "registrar";
        private const string ACTUALIZAR = "actualizar";
        private const string BORRAR = "borrar";
        #endregion

        #region Atributos
        private string strNombreApp;
        private string strError;
        private object vrUnico;
        private SqlParameter[] objDatosRegistro;
        private clsConexionBd objCnx;
        private DataSet dsDatos;
        #endregion

        #region Constructor
        public clsDirectorRN(string strNombreApp)
        {
            this.strNombreApp = strNombreApp;
            this.strError = string.Empty;
        }

        #endregion

        #region Propiedades
        public string Error { get => strError; }
        public object VrUnico { get => vrUnico; }
        public DataSet DsDatos { get => dsDatos; }

        #endregion

        #region Metodos Privados 
        private bool Validar(string validarDirector)
        {
            if (strNombreApp == string.Empty)
            {
                strError = "Olvidó enviar el nombre de la aplicación";
                return false;
            }
            switch (validarDirector)
            {
                case VALIDARSESION:
                    if (Contrasenas == string.Empty)
                    {
                        strError = "Debe ingresar una contraseña";
                        return false;
                    }
                    if (Usuarios == string.Empty)
                    {
                        strError = "Debe ingresar un usuario";
                        return false;
                    }
                    break;

            }
            return true;

        }     
        private bool agregarParametros(string metodoOrgien)
        {
            try
            {
                objDatosRegistro = new SqlParameter[1];
                switch (metodoOrgien.ToLower())
                {
                   
                    case VALIDARSESION:
                        if (!Validar(VALIDARSESION))
                        {
                            return false;
                        }
                        objDatosRegistro = new SqlParameter[2];
                        objDatosRegistro[0] = new SqlParameter("@usuario", Usuarios);
                        objDatosRegistro[1] = new SqlParameter("@contrasena", Contrasenas);
                        break;
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Metodos Publicos
     
        public override bool validarSesion()
        {
            try
            {
                if (!agregarParametros(VALIDARSESION))
                {
                    return false;
                }
                objCnx = new clsConexionBd(strNombreApp);
                objCnx.SQL = "SP_ValidaSesionDirector";
                objCnx.ParametrosSQL = objDatosRegistro;
                if (!objCnx.consultarValorUnico(true, true))
                {
                    strError = objCnx.Error;
                    objCnx.cerrarCnx();
                    objCnx = null;
                    return false;
                }
                vrUnico = objCnx.ValorUnico;
                objCnx.cerrarCnx();
                objCnx = null;
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
   
        public override bool actualizar()
        {
            try
            {
                if (!agregarParametros(ACTUALIZAR))
                {
                    return false;
                }
                objCnx = new clsConexionBd(strNombreApp);
                objCnx.SQL = "SP_actualizarDirector";
                objCnx.ParametrosSQL = objDatosRegistro;
                if (!objCnx.ejecutarSentencia(true, true))
                {
                    strError = objCnx.Error;
                    objCnx.cerrarCnx();
                    objCnx = null;
                    return false;
                }
                objCnx.cerrarCnx();
                objCnx = null;
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public override bool consultar()
        {
            try
            {
                objCnx = new clsConexionBd(strNombreApp);
                objCnx.SQL = "SP_ConsultarDirector";
                if (!objCnx.llenarDataSet(false, true))
                {
                    strError = objCnx.Error;
                    objCnx.cerrarCnx();
                    objCnx = null;
                    return false;
                }
                dsDatos = objCnx.DataSetLleno;
                objCnx.cerrarCnx();
                objCnx = null;
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
