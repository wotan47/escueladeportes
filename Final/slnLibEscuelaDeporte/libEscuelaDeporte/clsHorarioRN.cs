﻿using libConexionBd;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace libEscuelaDeporte
{
    public class clsHorarioRN
    {
        #region Constantes
        private const int CERO = 0;
        private const string VALIDARSESION = "validarsesion";
        private const string REGISTRAR = "registrar";
        private const string ACTUALIZAR = "actualizar";
        private const string BORRAR = "borrar";
        #endregion

        #region Atributos
        private string strNombreApp;
        private string strHoraInicio;
        private string strHoraFinalizacion;
        private string strDiasEntreno;
        private string strError;
        private object vrUnico;
        private SqlParameter[] objDatosRegistro;
        private clsConexionBd objCnx;
        private DataSet dsDatos;
        #endregion

        #region Constructor
        public clsHorarioRN(string strNombreApp)
        {
            this.strNombreApp = strNombreApp;
            this.strError = string.Empty;
        }

        #endregion

        #region Propiedades
        public string Error { get => strError; }
        public string HoraInicio { set => strHoraInicio = value; }
        public string HoraFinalizacion { set => strHoraFinalizacion = value; }
        public string DiasEntreno { set => strDiasEntreno = value; }
        public object VrUnico { get => vrUnico; }
        public DataSet DsDatos { get => dsDatos; }
        #endregion

        #region Metodos Privados 
        private bool Validar(string validarDirector)
        {
            if (strNombreApp == string.Empty)
            {
                strError = "Olvidó enviar el nombre de la aplicación";
                return false;
            }
            switch (validarDirector)
            {
                case REGISTRAR:
                    if (strHoraInicio == string.Empty)
                    {
                        strError = "Debe ingresar la hora de inicio";
                        return false;
                    }
                    if (strHoraFinalizacion == string.Empty)
                    {
                        strError = "Debe ingresar la hora de finalizacion";
                        return false;
                    }
                    if (strDiasEntreno == string.Empty)
                    {
                        strError = "Debe ingresar los dias de entreno";
                        return false;
                    }
                    break;
            }
            return true;
        }
        private bool agregarParametros(string metodoOrgien)
        {
            try
            {
                objDatosRegistro = new SqlParameter[1];
                switch (metodoOrgien.ToLower())
                {
                    case REGISTRAR:
                        if (!Validar(REGISTRAR))
                        {
                            return false;
                        }
                        objDatosRegistro = new SqlParameter[3];
                        objDatosRegistro[0] = new SqlParameter("@horaInicio", strHoraInicio);
                        objDatosRegistro[1] = new SqlParameter("@horaFin", strHoraFinalizacion);
                        objDatosRegistro[2] = new SqlParameter("@dias", strDiasEntreno);
                        break;
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Metodos Publicos
        public bool registrarHorario()
        {
            try
            {
                if (!agregarParametros(REGISTRAR))
                {
                    return false;
                }
                objCnx = new clsConexionBd(strNombreApp);
                //Cambiar sp
                objCnx.SQL = "crearHorario";
                objCnx.ParametrosSQL = objDatosRegistro;

                if (!objCnx.ejecutarSentencia(true, true))
                {
                    strError = objCnx.Error;
                    objCnx.cerrarCnx();
                    objCnx = null;
                    return false;
                }
                objCnx.cerrarCnx();
                objCnx = null;
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion
    }
}
