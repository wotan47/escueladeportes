﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace libParametrosCnx
{
    public class clsParametros
    {
        private const string CON_ = "CON_";
        private const string NODOSERVIDOR = "//Servidor";
        private const string NODOBASEDATOS = "//BaseDatos";
        private const string NODOSEGINTEGRADA = "//SeguridadIntegrada";
        private const string NO = "NO";
        private const string NODOUSUARIO = "//Usuario";
        private const string NODOCLAVE = "//Clave";
        private const string DATASOURCE = "Data source = ";
        private const string INITIALCATALOG = "Initial Catalog = ";
        private const string USERID = "User Id = ";
        private const string PASSWORD = "Password = ";
        private const string INTEGRATEDSECURITY = "Integrated Security = SSPI";

        #region "Atributos"
        private string strServidor;
        private string strBaseDatos;
        private string strUsuario;
        private string strClave;
        private string strSegInt;
        private string strPathXml;
        private string strCadCnx;
        private string strError;
        private XmlDocument objDoc;
        private XmlNode objNodo;

        #endregion

        #region "Constructor"
        public clsParametros()
        {
            strServidor = "";
            strBaseDatos = "";
            strUsuario = "";
            strClave = "";
            strSegInt = "";
            strPathXml = "";
            strCadCnx = "";
            strError = "";
            objDoc = new XmlDocument();
            objNodo = null;
        }
        #endregion

        #region "Propiedades"
        public string CadCnx { get => strCadCnx; }
        public string Error { get => strError; }
        #endregion

        #region "Métodos Privados"
        private bool validar(string nombreApp)
        {
            if (nombreApp == "")
            {
                strError = "Debe enviar el nombre de la aplicación para cargar el documento XML que tiene los parámetros de conexión";
                return false;
            }
            return true;
        }
        private bool cargarDatosXml(string nombreApp)
        {
            try
            {
                if (!validar(nombreApp))
                {
                    return false;
                }

                strPathXml = AppDomain.CurrentDomain.BaseDirectory + CON_ + nombreApp;

                objDoc.Load(strPathXml);

                objNodo = objDoc.SelectSingleNode(NODOSERVIDOR);
                strServidor = objNodo.InnerText;

                objNodo = objDoc.SelectSingleNode(NODOBASEDATOS);
                strBaseDatos = objNodo.InnerText;

                objNodo = objDoc.SelectSingleNode(NODOSEGINTEGRADA);
                strSegInt = objNodo.InnerText;

                if (strSegInt.ToUpper() == NO)
                {
                    objNodo = objDoc.SelectSingleNode(NODOUSUARIO);
                    strUsuario = objNodo.InnerText;

                    objNodo = objDoc.SelectSingleNode(NODOCLAVE);
                    strClave = objNodo.InnerText;
                }
                return true;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        #endregion

        #region "Métodos Públicos"
        public bool generarCadenaCnx(string nombreApp)
        {
            try
            {
                if (!cargarDatosXml(nombreApp))
                {
                    return false;
                }
                strCadCnx = DATASOURCE + strServidor + ";" + INITIALCATALOG + strBaseDatos + "; ";

                if (strSegInt.ToUpper() == NO)
                {
                    strCadCnx = strCadCnx + USERID + strUsuario + "; " + PASSWORD + strClave;
                }
                else
                {
                    strCadCnx = strCadCnx + INTEGRATEDSECURITY;
                }
                return true;

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        #endregion



    }
}
