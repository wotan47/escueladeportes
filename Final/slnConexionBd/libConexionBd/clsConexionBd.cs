﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using libParametrosCnx;

namespace libConexionBd
{
    public class clsConexionBd
    {
        #region "Atributos"
        private string strNombreApp;
        private string strCadenaCnx;
        private object objVrUnico;
        private string strSQL;
        private string strError;
        private bool blnHayCnx;
        private SqlConnection objCnxBD;
        private SqlCommand objCmd;
        private SqlDataReader objReader;
        private SqlDataAdapter objAdapter;
        private DataSet dsDatos;
        private SqlParameter[] objSqlParam;


        #endregion

        #region "Constructor"
        public clsConexionBd(string nombreAplicacion)
        {
            this.strNombreApp = nombreAplicacion;
            blnHayCnx = false;
            objCnxBD = new SqlConnection();
            objCmd = new SqlCommand();
            objAdapter = new SqlDataAdapter();
            dsDatos = new DataSet();
        }
        #endregion

        #region "Propiedades"
        public string SQL {set => strSQL = value; }
        public object ValorUnico { get => objVrUnico; }
        public SqlDataReader DataReaderLleno { get => objReader;}
        public DataSet DataSetLleno { get => dsDatos;}
        public string Error { get => strError;}
        public SqlParameter[] ParametrosSQL {set => objSqlParam = value; }
        #endregion

        #region "Métodos Privados"
        private bool validar(string metodoOrigen)
        {
            if(metodoOrigen.ToLower() == "generarcadenacnx")
            {
                if(this.strNombreApp.Trim() == string.Empty)
                {
                    strError = "No se envió el nombre de la aplicación. Sin este nombre no se puede realizar la conexión a la base de datos";
                    return false;
                }
            }
            else
            {
                if (string.IsNullOrEmpty(strSQL))
                {
                    strError = "Debe enviar la sentencia SQL para ejecutarla";
                    return false;
                }
            }
            return true;
        }

        private bool generarCadenaCnx()
        {
            try
            {
                if (!validar("generarCadenaCnx"))
                {
                    return false;
                }

                clsParametros objParam = new clsParametros();

                if (!objParam.generarCadenaCnx(strNombreApp))
                {
                    strError = objParam.Error;
                    objParam = null;
                    return false;
                }
                strCadenaCnx = objParam.CadCnx;
                objParam = null;
                return true;

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        private bool abrirCnx()
        {
            try
            {
                if (!generarCadenaCnx())
                {
                    return false;
                }
                objCnxBD.ConnectionString = strCadenaCnx;
                objCnxBD.Open();
                blnHayCnx = true;
                return true;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        private bool agregarParametros(SqlParameter[] objSqlParametros)
        {
            try
            {
                foreach(SqlParameter param in objSqlParametros)
                {
                    objCmd.Parameters.AddWithValue(param.ParameterName, param.Value);
                }
                return true;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        #endregion

        #region "Métodos Públicos"
        public bool cerrarCnx()
        {
            try
            {
                objCnxBD.Close();
                blnHayCnx = false;
                objCnxBD = null;
                return true;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public bool consultar(bool blnConParametros, bool blnProcAlmacenado)
        {
            try
            {
                if (!validar("consultar"))
                {
                    return false;
                }
                if (!blnHayCnx)
                {
                    if (!abrirCnx())
                    {
                        return false;
                    }
                }
                objCmd.Connection = objCnxBD;
                objCmd.CommandText = strSQL;

                if (blnProcAlmacenado)
                {
                    objCmd.CommandType = CommandType.StoredProcedure;
                    if (blnConParametros)
                    {
                        if (!agregarParametros(objSqlParam))
                        {
                            return false;
                        }
                    }
                }
                else
                {
                    objCmd.CommandType = CommandType.Text;
                }
                objReader = objCmd.ExecuteReader();
                return true;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public bool consultarValorUnico(bool blnConParametros, bool blnProcAlmacenado)
        {
            try
            {
                if (!validar("consultarValorUnico"))
                {
                    return false;
                }
                if (!blnHayCnx)
                {
                    if (!abrirCnx())
                    {
                        return false;
                    }
                }
                objCmd.Connection = objCnxBD;
                objCmd.CommandText = strSQL;

                if (blnProcAlmacenado)
                {
                    objCmd.CommandType = CommandType.StoredProcedure;
                    if (blnConParametros)
                    {
                        if (!agregarParametros(objSqlParam))
                        {
                            return false;
                        }
                    }
                }
              
                objVrUnico = objCmd.ExecuteScalar();
                return true;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public bool ejecutarSentencia(bool blnConParametros, bool blnProcAlmacenado)
        {
            try
            {
                if (!validar("ejecutarSentencia"))
                {
                    return false;
                }
                if (!blnHayCnx)
                {
                    if (!abrirCnx())
                    {
                        return false;
                    }
                }
                objCmd.Connection = objCnxBD;
                objCmd.CommandText = strSQL;

                if (blnProcAlmacenado)
                {
                    objCmd.CommandType = CommandType.StoredProcedure;
                    if (blnConParametros)
                    {
                        if (!agregarParametros(objSqlParam))
                        {
                            return false;
                        }
                    }
                }
              
                objCmd.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public bool llenarDataSet(bool blnConParametros, bool blnProcAlmacenado)
        {
            try
            {
                if (!validar("llenarDataSet"))
                {
                    return false;
                }

                if (!blnHayCnx)
                {
                    if (!abrirCnx())
                    {
                        return false;
                    }
                }
                objCmd.Connection = objCnxBD;
                objCmd.CommandText = strSQL;

                if (blnProcAlmacenado)
                {
                    objCmd.CommandType = CommandType.StoredProcedure;
                    if (blnConParametros)
                    {
                        if (!agregarParametros(objSqlParam))
                        {
                            return false;
                        }
                    }
                }
                objAdapter.SelectCommand = objCmd;
                objAdapter.Fill(dsDatos);
                return true;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        #endregion
    }
}
