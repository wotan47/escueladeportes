﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using libEscuelaDeporte;
using libLlenarGrids;

namespace libEscuelaDeporteOP
{
   public class clsAcudienteOP
    {
        #region Constantes
        private const int CERO = 0;
        private const string VALIDARSESION = "validarsesion";
        private const string REGISTRAR = "registrar";
        private const string ACTUALIZAR = "actualizar";
        private const string BORRAR = "borrar"; 
        #endregion

        #region Atributos
        private string strNombreApp;
        private string strError;
        private string strCorreo;
        private string strDireccion;
        private string strCelular;
        private string strContrasena;
        private string strUsuario;
        private string strOcupacion;
        private object vrUnico;
        #endregion

        #region Constructor
        public clsAcudienteOP(string strNombreApp)
        {
            this.strNombreApp = strNombreApp;
            this.strError = string.Empty;
            this.strCorreo = string.Empty;
            this.strDireccion = string.Empty;
            this.strCelular = string.Empty;
            this.strContrasena = string.Empty;
            this.strUsuario = string.Empty;
            this.strOcupacion = string.Empty;
        }
        #endregion

        #region Propiedades

        public string NombreApp { set => strNombreApp = value; }
        public string Error { get => strError; }
        public string Correo { set => strCorreo = value; get => strCorreo; }
        public string Direccion { set => strDireccion = value; get => strDireccion; }
        public string Celular { set => strCelular = value; get => strCelular; }
        public string Ocupacion { set => strOcupacion = value; get => strOcupacion; }
        public string Usuario { get => strUsuario; set => strUsuario = value; }
        public string Contrasena { get => strContrasena; set => strContrasena = value; }

        public object VrUnico { get => vrUnico; }
        #endregion

        #region Metodos Privados
       
        #endregion

        #region Metodos Publicos
        public bool actualizarOP()
        {
            try
            {
                clsAcudienteRN acudienteRN = new clsAcudienteRN(strNombreApp);
                acudienteRN.Correos = strCorreo;
                acudienteRN.Direcciones = strDireccion;
                acudienteRN.Celulares = strCelular;
                acudienteRN.Ocupacion = strOcupacion;
                acudienteRN.Usuarios = strUsuario;
                acudienteRN.Contrasenas = strContrasena;                
                if (!acudienteRN.actualizar())
                {
                    strError = acudienteRN.Error;
                    acudienteRN = null;
                    return false;
                }
                acudienteRN = null;
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public bool consultarOP()
        {
            try
            {
                clsAcudienteRN acudiente = new clsAcudienteRN(strNombreApp);
                if (!acudiente.consultar())
                {
                    strError = acudiente.Error;
                    acudiente = null;
                    return false;
                }
                
                strCorreo = acudiente.DsDatos.Tables[0].Rows[0]["correo"].ToString();
                strDireccion = acudiente.DsDatos.Tables[0].Rows[0]["direccion"].ToString();
                strCelular = acudiente.DsDatos.Tables[0].Rows[0]["celular"].ToString();
                strOcupacion = acudiente.DsDatos.Tables[0].Rows[0]["ocupacion"].ToString();
                strUsuario = acudiente.DsDatos.Tables[0].Rows[0]["usuario"].ToString();
                strContrasena = acudiente.DsDatos.Tables[0].Rows[0]["contrasena"].ToString();
                acudiente = null;
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool validarSesionOP()
        {
            try
            {
               
                clsAcudienteRN acudienteRN = new clsAcudienteRN(strNombreApp);
                acudienteRN.Usuarios = strUsuario;
                acudienteRN.Contrasenas = strContrasena;
                if (!acudienteRN.validarSesion())
                {
                    strError = acudienteRN.Error;
                    acudienteRN = null;
                    return false;
                }
                vrUnico = acudienteRN.VrUnico;
                acudienteRN = null;
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion
    }
}
