﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using libEscuelaDeporte;
using System.Web.UI.WebControls;
using libLlenarGrids;
using System.Data;

namespace libEscuelaDeporteOP
{
  public  class clsListaClaseOP
    {
        #region Constantes
        private const string REGISTRAR = "registrar";
        private const int CERO = 0;
        #endregion

        #region Atributos
        private int intIdGrupo;
        private string strDocumentoEstudiante;
        private string strError;
        private string strNombreApp;
        #endregion

        #region Constructor

        public clsListaClaseOP(string strNombreApp)
        {
            
            this.strDocumentoEstudiante = string.Empty;
            this.strError = string.Empty;
            this.strNombreApp = strNombreApp;
        }

        #endregion

        #region Propiedades
        public int IdGrupo { set => intIdGrupo = value; }
        public string Documento { set => strDocumentoEstudiante = value; }
        public string Error { get => strError; }
        #endregion

        #region Metodos Privados
        private bool Validar(string validar)
        {
            switch (validar)
            {
                case REGISTRAR:
                    if (strDocumentoEstudiante == string.Empty)
                    {
                        strError = "Debe ingresar un documento";
                        return false;
                    }
                    if (intIdGrupo <= CERO)
                    {
                        strError = "el id de grupo no puede ser inferior o igual que 0";
                        return false;
                    }
                break;
                
            }
            return true;
        }
       
        #endregion

        #region Metodos Publicos
        public bool registrarOP()
        {
            try
            {
                if (!Validar(REGISTRAR))
                {
                    return false;
                }
                clsListaClaseRN listaClaseRN = new clsListaClaseRN(strNombreApp);
                listaClaseRN.IdGrupo = intIdGrupo;
                listaClaseRN.Documento = strDocumentoEstudiante;
                if (!listaClaseRN.registrar())
                {
                    strError = listaClaseRN.Error;
                    listaClaseRN = null;
                    return false;
                }
                listaClaseRN = null;
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        #endregion

    }
}
