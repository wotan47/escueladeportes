﻿using libEscuelaDeporte;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace libEscuelaDeporteOP
{
    public class clsHorarioOP
    {
        #region Constantes
        private const int CERO = 0;
        private const string VALIDARSESION = "validarsesion";
        private const string REGISTRAR = "registrar";
        private const string ACTUALIZAR = "actualizar";
        private const string BORRAR = "borrar";
        #endregion

        #region Atributos
        private string strHoraInicio;
        private string strNombreApp;
        private string strError;
        private string strHoraFinalizacion;
        private string strDiasEntreno;
        private object vrUnico;
        #endregion

        #region Constructor
        public clsHorarioOP(string strNombreApp)
        {
            this.strNombreApp = strNombreApp;
            this.strError = string.Empty;
            this.strHoraInicio = string.Empty;
            this.strHoraFinalizacion = string.Empty;
            this.strDiasEntreno = string.Empty;
        }
        #endregion

        #region Propiedades

        public string NombreApp { set => strNombreApp = value; }
        public string Error { get => strError; }
        public string HoraInicio { set => strHoraInicio = value; }
        public string HoraFinalizacion { set => strHoraFinalizacion = value; }
        public string DiasEntreno { set => strDiasEntreno = value; }
        public object VrUnico { get => vrUnico; }
        #endregion

        #region Metodos Privados
        private bool Validar(string validarDirector)
        {
            switch (validarDirector)
            {
                case REGISTRAR:
                    if (strHoraInicio == string.Empty)
                    {
                        strError = "Debe ingresar la hora de inicio";
                        return false;
                    }
                    if (strHoraFinalizacion == string.Empty)
                    {
                        strError = "Debe ingresar la hora de finalizacion";
                        return false;
                    }
                    if (strDiasEntreno == string.Empty)
                    {
                        strError = "Debe ingresar los dias de entreno";
                        return false;
                    }
                    break;
            }
            return true;
        }
       
        #endregion

        #region Metodos Publicos
        public bool registrarOP()
        {
            try
            {
                if (!Validar(REGISTRAR))
                {
                    return false;
                }
                clsHorarioRN HorarioRN = new clsHorarioRN(strNombreApp);
                HorarioRN.HoraInicio = strHoraInicio;
                HorarioRN.HoraFinalizacion = strHoraFinalizacion;
                HorarioRN.DiasEntreno = strDiasEntreno;
                if (!HorarioRN.registrarHorario())
                {
                    strError = HorarioRN.Error;
                    HorarioRN = null;
                    return false;
                }
                HorarioRN = null;
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
       /* public bool borrarOP()
        {
            try
            {
                if (!Validar(BORRAR))
                {
                    return false;
                }
                clsPadreRN padreRN = new clsPadreRN(strNombreApp);
                padreRN.Documento = strDocumento;
                if (!padreRN.borrar())
                {
                    strError = padreRN.Error;
                    padreRN = null;
                    return false;
                }
                padreRN = null;
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }*/

       /*public bool consultarOP(GridView gvPadre)
        {
            try
            {
                clsPadreRN padre = new clsPadreRN(strNombreApp);
                if (!padre.consultar())
                {
                    strError = padre.Error;
                    padre = null;
                    return false;
                }
                if (!llenarGrid(gvPadre, padre.DsDatos.Tables[0]))
                {
                    padre = null;
                    return false;
                }
                padre = null;
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool validarSesionOP()
        {
            try
            {
                if (!Validar(VALIDARSESION))
                {
                    return false;
                }
                clsPadreRN padreRN = new clsPadreRN(strNombreApp);
                padreRN.Usuario = strUsuario;
                padreRN.Contasena = strContasena;
                if (!padreRN.validarSesion())
                {
                    strError = padreRN.Error;
                    padreRN = null;
                    return false;
                }
                vrUnico = padreRN.VrUnico;
                padreRN = null;
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }*/
        #endregion
    }
}
