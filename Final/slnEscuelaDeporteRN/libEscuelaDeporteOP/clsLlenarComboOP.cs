﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using libEscuelaDeporte;

namespace libEscuelaDeporteOP
{
   public class clsLlenarComboOP
    {
        private const int MENOSUNO = -1;

        #region Atributos
        private string strTipoConsulta;
        private string strNombreApp;
        private string strDeporte;
        private int intIdHorario;
        private string strError;
        private DropDownList ddlGen;
        #endregion

        #region Contructor
        public clsLlenarComboOP(string strNombreApp)
        {
            this.strTipoConsulta = string.Empty;
            this.strNombreApp = strNombreApp;
            this.strDeporte = string.Empty;
            this.intIdHorario = MENOSUNO;
            this.strError = string.Empty;
        }
        #endregion

        #region Propiedades
        public string TipoConsulta { set => strTipoConsulta = value; }
        public string Deporte {set => strDeporte = value; }
        public int IdHorario {  set => intIdHorario = value; }
        public string Error { get => strError; }
        public DropDownList DdlGen { set => ddlGen = value; }
        #endregion

        #region Metodos Privados
        private bool Validar()
        {
            if (strDeporte == string.Empty)
            {
                strError = "Debe ingresar un deporte";
                return false;
            }
            return true;
        }
        #endregion

        #region Metodos Publicos
        public bool llenarDrop()
        {
            try
            {
                clsLlenarComboRN llenarComboRN = new clsLlenarComboRN(strNombreApp);

                switch (ddlGen.ID.ToLower())
                {
                    case "ddlescenario":
                        if (!Validar())
                        {
                            return false;
                        }
                        llenarComboRN.Deporte = strDeporte;
                        if (!llenarComboRN.llenarDropDownEscenario(ddlGen))
                        {
                            strError = llenarComboRN.Error;
                            llenarComboRN = null;
                            return false;
                        }
                        break;
                    case "ddlprofesor":
                        if (!llenarComboRN.llenarDropDownProfesor(ddlGen))
                        {
                            strError = llenarComboRN.Error;
                            llenarComboRN = null;
                            return false;
                        }
                        break;
                    case "ddlgrupo":
                        if (!llenarComboRN.llenarDropDownGrupo(ddlGen))
                        {
                            strError = llenarComboRN.Error;
                            llenarComboRN = null;
                            return false;
                        }
                        break;
                    case "ddlestudiante":
                        if (!llenarComboRN.llenarDropDownEstudiante(ddlGen))
                        {
                            strError = llenarComboRN.Error;
                            llenarComboRN = null;
                            return false;
                        }
                        break;
                    default:
                        if (!llenarComboRN.llenarDropDowns(ddlGen))
                        {
                            strError = llenarComboRN.Error;
                            llenarComboRN = null;
                            return false;
                        }
                        break;
                }
                llenarComboRN = null;
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
