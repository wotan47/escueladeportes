﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using libEscuelaDeporte;
using libLlenarGrids;

namespace libEscuelaDeporteOP
{
    public class clsDirectorOP
    {
        #region CONSTANTES
        private const int CERO = 0;
        private const string BORRAR = "borrar";
        private const string ACTUALIZAR = "actualizar";
        private const string REGISTRAR = "registrar";
        private const string VALIDARSESION = "validarSesion";
        #endregion

        #region Atributos
        private string strNombreApp;
        private string strError;
        private string strDocumento;
        private string strNombre;
        private string strApellido;
        private string strContasena;
        private string strUsuario;
        private int intEdad;
        private int intTelefono;
        private object vrUnico;
        #endregion

        #region Constructor
        public clsDirectorOP(string strNombreApp)
        {
            this.strNombreApp = strNombreApp;
            this.strError = string.Empty;
            this.strDocumento = string.Empty;
            this.strNombre = string.Empty;
            this.strApellido = string.Empty;
            this.strContasena = string.Empty;
            this.strUsuario = string.Empty;
            this.intEdad = CERO;
            this.intTelefono = CERO;
        }

        #endregion

        #region Propiedades

        public string NombreApp {  set => strNombreApp = value; }
        public string Error { get => strError; }
        public string Documento {  set => strDocumento = value; }
        public string Nombre { set => strNombre = value; }
        public string Apellido {  set => strApellido = value; }
        public string Contasena { set => strContasena = value; }
        public string Usuario { set => strUsuario = value; }
        public int Edad { set => intEdad = value; }
        public int Telefono { set => intTelefono = value; }
        public object VrUnico { get => vrUnico; }
        #endregion

        #region Metodos Privados
        private bool Validar(string validarDirector)
        {
            switch (validarDirector)
            {
                case VALIDARSESION:
                    if (strContasena == string.Empty)
                    {
                        strError = "Debe ingresar una contraseña";
                        return false;
                    }
                    if (strUsuario == string.Empty)
                    {
                        strError = "Debe ingresar un usuario";
                        return false;
                    }
                    break;
                case REGISTRAR:
                    if (strDocumento == string.Empty)
                    {
                        strError = "Debe ingresar el documento";
                        return false;
                    }
                    if (strNombre == string.Empty)
                    {
                        strError = "Debe ingresar el nombre";
                        return false;
                    }
                    if (strApellido == string.Empty)
                    {
                        strError = "Debe ingresar el apellido";
                        return false;
                    }
                    if (strContasena == string.Empty)
                    {
                        strError = "Debe ingresar una contraseña";
                        return false;
                    }
                    if (strUsuario == string.Empty)
                    {
                        strError = "Debe ingresar un usuario";
                        return false;
                    }
                    if (intEdad <= CERO)
                    {
                        strError = "La edad no puede ser menor o igual a 0";
                        return false;
                    }
                    break;
                case ACTUALIZAR:
                    if (strDocumento == string.Empty)
                    {
                        strError = "Debe ingresar el documento";
                        return false;
                    }
                    if (strNombre == string.Empty)
                    {
                        strError = "Debe ingresar el nombre";
                        return false;
                    }
                    if (strApellido == string.Empty)
                    {
                        strError = "Debe ingresar el apellido";
                        return false;
                    }
                    if (intEdad <= CERO)
                    {
                        strError = "La edad no puede ser menor o igual a 0";
                        return false;
                    }
                    if (intTelefono < CERO)
                    {
                        strError = "El telefono no puede ser menor o igual a 0";
                        return false;
                    }
                    break;
                case BORRAR:
                    if (strDocumento == string.Empty)
                    {
                        strError = "Debe ingresar el documento";
                        return false;
                    }
                    break;
            }
            return true;
        }
        private bool llenarGrid(GridView gvGenerico, DataTable dtDatos)
        {
            try
            {
                clsLlenarGrids llenarGrids = new clsLlenarGrids();
                if (!llenarGrids.llenarGridWeb(gvGenerico, dtDatos))
                {
                    strError = llenarGrids.Error;
                    llenarGrids = null;
                    return false;
                }
                llenarGrids = null;
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Metodos Publicos
        
    
        public bool consultarOP(GridView gvDirector)
        {
            try
            {
                clsDirectorRN director = new clsDirectorRN(strNombreApp);

                if (!director.consultar())
                {
                    strError = director.Error;
                    director = null;
                    return false;
                }
                if (!llenarGrid(gvDirector, director.DsDatos.Tables[0]))
                {
                    director = null;
                    return false;
                }
                director = null;
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool validarSesionOP()
        {
            try
            {
                if (!Validar(VALIDARSESION))
                {
                    return false;
                }
                clsDirectorRN directorRN = new clsDirectorRN(strNombreApp);
                directorRN.Usuarios = strUsuario;
                directorRN.Contrasenas = strContasena;
                if (!directorRN.validarSesion())
                {
                    strError = directorRN.Error;
                    directorRN = null;
                    return false;
                }
                vrUnico = directorRN.VrUnico;
                directorRN = null;
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
