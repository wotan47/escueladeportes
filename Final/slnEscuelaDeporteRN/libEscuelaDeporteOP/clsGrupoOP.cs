﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using libEscuelaDeporte;
using System.Web.UI.WebControls;
using libLlenarGrids;
using System.Data;

namespace libEscuelaDeporteOP
{
  public  class clsGrupoOP
    {
        #region Constantes
        private const int MENOSUNO = -1;
        private const string REGISTRAR = "registrar";
        private const int CERO = 0;
        private const string BORRAR = "borrar";
        private const string ACTUALIZAR = "actualizar"; 
        #endregion

        #region Atributos
        private int intIdGrupo;
        private int intIdHorario;
        private int intIdEscenario;
        private string strDeporte;
        private string strDocumento;
        private string strError;
        private string strNombreApp;
        #endregion

        #region Constructor

        public clsGrupoOP(string strNombreApp)
        {
            this.intIdHorario = MENOSUNO;
            this.intIdEscenario = MENOSUNO;
            this.strDeporte = string.Empty;
            this.strDocumento = string.Empty;
            this.strError = string.Empty;
            this.strNombreApp = strNombreApp;
        }

        #endregion

        #region Propiedades
        public int IdGrupo {  set => intIdGrupo = value; }
        public int IdHorario { set => intIdHorario = value; }
        public int IdEscenario {  set => intIdEscenario = value; }
        public string Deporte {  set => strDeporte = value; }
        public string Documento { set => strDocumento = value; }
        public string Error { get => strError; }
        #endregion

        #region Metodos Privados
        private bool Validar(string validar)
        {
            switch (validar)
            {
                case REGISTRAR:
                    if (strDocumento == string.Empty)
                    {
                        strError = "Debe ingresar un documento";
                        return false;
                    }
                    if (intIdEscenario <= CERO)
                    {
                        strError = "el id de escenario no puede ser inferior o igual que 0";
                        return false;
                    }
                    if (strDeporte == string.Empty)
                    {
                        strError = "el id de deporte no puede ser inferior o igual que 0";
                        return false;
                    }
                    if (intIdHorario <= CERO)
                    {
                        strError = "el id de horario no puede ser inferior o igual que 0";
                        return false;
                    }
                    break;
                case BORRAR:
                    if (intIdGrupo <= CERO)
                    {
                        strError = "el id de grupo no puede ser inferior o igual que 0";
                        return false;
                    }
                break;
                case ACTUALIZAR:
                    if (strDocumento == string.Empty)
                    {
                        strError = "Debe ingresar un documento";
                        return false;
                    }
                    if (intIdEscenario <= CERO)
                    {
                        strError = "el id de escenario no puede ser inferior o igual que 0";
                        return false;
                    }
                    if (strDeporte == string.Empty)
                    {
                        strError = "el id de deporte no puede ser inferior o igual que 0";
                        return false;
                    }
                    if (intIdHorario <= CERO)
                    {
                        strError = "el id de horario no puede ser inferior o igual que 0";
                        return false;
                    }
                    if (intIdGrupo <= CERO)
                    {
                        strError = "el id de grupo no puede ser inferior o igual que 0";
                        return false;
                    }
                    break;
            }
            return true;
        }
        private bool llenarGrid(GridView gvGenerico, DataTable dtDatos)
        {
            try
            {
                clsLlenarGrids llenarGrids = new clsLlenarGrids();
                if (!llenarGrids.llenarGridWeb(gvGenerico, dtDatos))
                {
                    strError = llenarGrids.Error;
                    llenarGrids = null;
                    return false;
                }
                llenarGrids = null;
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Metodos Publicos
        public bool registrarOP()
        {
            try
            {
                if (!Validar(REGISTRAR))
                {
                    return false;
                }
                clsGrupoRN grupoRN = new clsGrupoRN(strNombreApp);
                grupoRN.IdHorario = intIdHorario;
                grupoRN.IdEscenario = intIdEscenario;
                grupoRN.Deporte = strDeporte;
                grupoRN.Documento = strDocumento;
                if (!grupoRN.registrar())
                {
                    strError = grupoRN.Error;
                    grupoRN = null;
                    return false;
                }
                grupoRN = null;
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool borrarOP()
        {
            try
            {
                if (!Validar(BORRAR))
                {
                    return false;
                }
                clsGrupoRN grupoRN = new clsGrupoRN(strNombreApp);
                grupoRN.IdGrupo = intIdGrupo;
                if (!grupoRN.borrar())
                {
                    strError = grupoRN.Error;
                    grupoRN = null;
                    return false;
                }
                grupoRN = null;
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool actualizarOP()
        {
            try
            {
                if (!Validar(ACTUALIZAR))
                {
                    return false;
                }
                clsGrupoRN grupoRN = new clsGrupoRN(strNombreApp);
                grupoRN.IdHorario = intIdHorario;
                grupoRN.IdEscenario = intIdEscenario;
                grupoRN.Deporte = strDeporte;
                grupoRN.Documento = strDocumento;
                grupoRN.IdGrupo = intIdGrupo;
                if (!grupoRN.actualizar())
                {
                    strError = grupoRN.Error;
                    grupoRN = null;
                    return false;
                }
                grupoRN = null;
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool consultarOP(GridView gvGrupo)
        {
            try
            {
                clsGrupoRN grupo = new clsGrupoRN(strNombreApp);

                if (!grupo.consultar())
                {
                    strError = grupo.Error;
                    grupo = null;
                    return false;
                }
                if (!llenarGrid(gvGrupo, grupo.DsDatos.Tables[0]))
                {
                    grupo = null;
                    return false;
                }
                grupo = null;
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

    }
}
