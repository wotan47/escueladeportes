﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frmLogin.aspx.cs" Inherits="prjEscuelaDeporte.frmLogin" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
 <link href="css/bootstrap.min.css" rel="stylesheet" />
 <link href="css/login.css" rel="stylesheet" />
 <link href="img/fond.jpg" rel="icon" />
    <title>Login</title>
</head>
<body class="bg-light">
    <form id="frmLogin" runat="server">
        <div class="container">
            <div class="card">
                <asp:Panel runat="server" ID="pnlDirector">
                    <div class="card-header text-center bg-primary">
                        <h5>Director</h5>
                    </div>
                </asp:Panel>
                <asp:Panel runat="server" ID="PnlProfesor">
                    <div class="card-header text-center bg-success">
                        <h5>Profesor</h5>
                    </div>
                </asp:Panel>
                <asp:Panel runat="server" ID="pnlEstudiante">
                    <div class="card-header text-center bg-warning">
                        <h5>Estudiante</h5>
                    </div>
                </asp:Panel>
                <asp:Panel runat="server" ID="pnlAcudiente">
                    <div class="card-header text-center bg-danger">
                        <h5>Acudiente</h5>
                    </div>
                </asp:Panel>
                <asp:Panel runat="server" ID="pnlPrincipal">
                    <div class="card-body">
                        <div class="col-sm-10 offset-1">
                            <div class="input-group mb-3 inputs">
                                <div class="input-group-prepend">
                                    <span class="input-group-text fas fa-user pt-2"></span>
                                </div>
                                <asp:TextBox ID="txtUsuario" runat="server" CssClass="form-control" placeholder="Usuario" />
                            </div>
                        </div>
                        <div class="col-sm-10 offset-1">
                            <div class="input-group mb-3 inputs">
                                <div class="input-group-prepend">
                                    <span class="input-group-text fas fa-lock pt-2"></span>
                                </div>
                                <asp:TextBox ID="txtContraseña" TextMode="Password" runat="server" CssClass="form-control" placeholder="Contraseña"  />
                            </div>
                        </div>
                    </div>
                </asp:Panel>

                <asp:Panel runat="server" ID="pnlBtnDirector">
                    <div class="col-sm-6 offset-3">
                        <asp:Button Text="Ingresar" ID="btnIngresarDirector" CssClass="btn btn-block btn-primary" runat="server" OnClick="btnIngresarDirector_Click" />
                                            <a class="col-sm-6 text-center">Olvidé contraseña</a>
                    </div>
                </asp:Panel>
                <asp:Panel runat="server" ID="pnlBtnProfesor">
                    <div class="col-sm-6 offset-3">
                        <asp:Button Text="Ingresar" ID="btnIngresarProfesor" CssClass="btn btn-block btn-success" runat="server" OnClick="btnIngresarProfesor_Click" />
                                            <a class="col-sm-6 text-center">Olvidé contraseña</a>
                    </div>
                </asp:Panel>
                <asp:Panel runat="server" ID="pnlBtnEstudiante">
                    <div class="col-sm-6 offset-3">
                        <asp:Button Text="Ingresar" ID="btnIngresarEstudiante" CssClass="btn btn-block btn-warning" runat="server" OnClick="btnIngresarEstudiante_Click" />
                                            <a class="col-sm-6 text-center">Olvidé contraseña</a>
                    </div>
                </asp:Panel>
                <asp:Panel runat="server" ID="pnlBtnAcudiente">
                    <div class="col-sm-6 offset-3">
                        <asp:Button Text="Ingresar" ID="btnIngresarPadre" CssClass="btn btn-block btn-danger" runat="server" OnClick="btnIngresarPadre_Click" />
                                            <a class="col-sm-6 text-center">Olvidé contraseña</a>
                    </div>
                </asp:Panel>
                <br />

                <asp:Label ID="lblMensaje" runat="server" Visible="false" CssClass="text-center"></asp:Label>
                <br />
                <div class="card-footer ml-3">
                    <asp:ImageButton ImageUrl="img/Admin.png" ID="btnDirector" runat="server" CssClass="imagenes admin" OnClick="btnDirector_Click" />
                    <asp:ImageButton ImageUrl="img/docente.png" ID="btnDocente" runat="server" CssClass="imagenes docente" OnClick="btnDocente_Click" />
                    <asp:ImageButton ImageUrl="img/estudiante.png" ID="btnEstudiante" runat="server" CssClass="imagenes estudiante" OnClick="btnEstudiante_Click" />
                    <asp:ImageButton ImageUrl="img/padres.png" ID="btnPadre" runat="server" CssClass="imagenes padre" OnClick="btnPadre_Click" />
                </div>
            </div>
        </div>
        
    </form>
    <script src="https://kit.fontawesome.com/c40c816a13.js" crossorigin="anonymous"></script>
    <script src="js/jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</body>
</html>
