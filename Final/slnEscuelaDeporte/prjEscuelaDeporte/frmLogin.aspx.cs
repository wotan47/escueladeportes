﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Reflection;
using libEscuelaDeporteOP;

namespace prjEscuelaDeporte
{
    public partial class frmLogin : System.Web.UI.Page
    {
        #region "Variables Globales"
        private static string strNombreApp;
        private object vrUnico;
        #endregion

        #region Metodos privados
        private void mostrarMsj(string msj, bool error)
        {
            this.lblMensaje.Text = msj;

            if (msj == string.Empty)
            {
                this.lblMensaje.Visible = false;
                return;
            }
            this.lblMensaje.Visible = true;

            if (error)
            {
                this.lblMensaje.CssClass = "label label-danger h5";
            }
            else
            {
                this.lblMensaje.CssClass = "label label-success h5";
            }
        }
        private bool validar()
        {
            if (this.txtUsuario.Text.Trim() == string.Empty)
            {
                mostrarMsj("Debe ingresar el Usuario", true);
                return false;
            }
            if (this.txtContraseña.Text.Trim() == string.Empty)
            {
                mostrarMsj("Debe ingresar la Contraseña", true);
                return false;
            }
            return true;
        }
        private void mostrarPanel(string panel)
        {
            switch (panel)
            {
                case "pnlDirector":
                    pnlDirector.Visible = true;
                    pnlPrincipal.Visible = true;
                    pnlBtnDirector.Visible = true;
                    break;
                case "pnlEstudiante":
                    pnlEstudiante.Visible = true;
                    pnlPrincipal.Visible = true;
                    pnlBtnEstudiante.Visible = true;
                    break;
                case "pnlProfesor":
                    PnlProfesor.Visible = true;
                    pnlPrincipal.Visible = true;
                    pnlBtnProfesor.Visible = true;
                    break;
                case "pnlAcudiente":
                    pnlAcudiente.Visible = true;
                    pnlPrincipal.Visible = true;
                    pnlBtnAcudiente.Visible = true;
                    break;
                case "inicio":
                    pnlDirector.Visible = false;
                    pnlPrincipal.Visible = false;
                    pnlEstudiante.Visible = false;
                    pnlAcudiente.Visible = false;
                    PnlProfesor.Visible = false;
                    pnlBtnDirector.Visible = false;
                    pnlBtnEstudiante.Visible = false;
                    pnlBtnAcudiente.Visible = false;
                    pnlBtnProfesor.Visible = false;
                    break;
            }
        }
        private void Limpiar()
        {
            this.txtUsuario.Text = string.Empty;
            this.txtContraseña.Text = string.Empty;
        }
        private void iniciarSesion(string perfil)
        {
            try
            {
                if (!validar())
                {
                    return;
                }
                switch (perfil)
                {
                    case "director":
                        clsDirectorOP director = new clsDirectorOP(strNombreApp);
                        director.Usuario = this.txtUsuario.Text.Trim();
                        director.Contasena = this.txtContraseña.Text.Trim();
                        if (!director.validarSesionOP())
                        {
                            mostrarMsj(director.Error, true);
                            director = null;
                            return;
                        }
                        vrUnico = director.VrUnico;
                        director = null;
                        if (vrUnico != null)
                        {
                            if (vrUnico.ToString() == this.txtUsuario.Text)
                            {
                                Server.Transfer("frmDirector.aspx");
                            }
                        }
                        break;
                    
                    case "acudiente":
                        clsAcudienteOP acudiente = new clsAcudienteOP(strNombreApp);
                        acudiente.Usuario = this.txtUsuario.Text.Trim();
                        acudiente.Contrasena = this.txtContraseña.Text.Trim();
                        if (!acudiente.validarSesionOP())
                        {
                            mostrarMsj(acudiente.Error, true);
                            acudiente = null;
                            return;
                        }
                        vrUnico = acudiente.VrUnico;
                        acudiente = null;
                        if (vrUnico != null)
                        {
                            if (vrUnico.ToString() == this.txtUsuario.Text)
                            {
                                Server.Transfer("frmPerfilAcudiente.aspx");
                            }
                        }
                        break;
                }
            }
            catch (Exception ex)
            {

                mostrarMsj(ex.Message, true);
            }
                                   
        }
        #endregion

        #region Eventos
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                strNombreApp = Assembly.GetExecutingAssembly().GetName().Name + ".xml";
            }
            mostrarPanel("inicio");
        }
        protected void btnDirector_Click(object sender, ImageClickEventArgs e)
        {
            mostrarPanel("pnlDirector");
        }
        protected void btnPadre_Click(object sender, ImageClickEventArgs e)
        {
            mostrarPanel("pnlAcudiente");
        }
        protected void btnEstudiante_Click(object sender, ImageClickEventArgs e)
        {
            mostrarPanel("pnlEstudiante");
        }
        protected void btnDocente_Click(object sender, ImageClickEventArgs e)
        {
            mostrarPanel("pnlProfesor");
        }
        protected void btnIngresarDirector_Click(object sender, EventArgs e)
        {
            iniciarSesion("director");
            Limpiar();
        }
        protected void btnIngresarProfesor_Click(object sender, EventArgs e)
        {
            
        }
        protected void btnIngresarEstudiante_Click(object sender, EventArgs e)
        {
            
        }
        protected void btnIngresarPadre_Click(object sender, EventArgs e)
        {
            iniciarSesion("acudiente");
            Limpiar();
        }
        #endregion
    }
}