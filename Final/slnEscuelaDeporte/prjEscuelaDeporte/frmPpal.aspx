﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frmPpal.aspx.cs" Inherits="prjEscuelaDeporte.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <link href="css/app.css" rel="stylesheet" />
    <link href="img/fond.jpg" rel="icon" />
    <title>Escuela de deporte</title>
</head>
<body>
    <form id="frmPpal" runat="server">
        <div class="jumbotron">
            <h1>Escuela de deportes</h1>
        </div>
        
        <div class="container contenedorPrincipal">
            <center>
                <asp:Button PostBackUrl="~/frmLogin.aspx" Text="Iniciar sesión" runat="server" CssClass="btn btn-outline-light iniciar" />
            </center>
        </div>
    </form>

    <script src="js/jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</body>
</html>
