﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frmSeleccionarGrupoAcudiente.aspx.cs" Inherits="prjEscuelaDeporte.frmSeleccionarGA" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <link href="img/fond.jpg" rel="icon" />
    <link href="css/grupo.css" rel="stylesheet" />
    <title>Seleccionar grupo</title>
</head>
<body>
    <form id="frmSeleccionarGrupo" runat="server">
        <nav class="navbar navbar-expand-md fixed-top navbar-dark bg-dark">
            <a href="frmPerfilAcudiente.aspx" class="navbar-brand">Escuela de deportes</a>
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a href="#" class="nav-link">Crear estudiante</a>
                </li>
                <li class="nav-item">
                    <a href="frmSeleccionarGrupoAcudiente.aspx" class="nav-link">Seleccionar grupo</a>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link">Autorizar grupo</a>
                </li>
                <li class="nav-item dropdown ">
                    <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" aria-expanded="true">Perfil</a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item" href="frmEditarPerfilAcudiente.aspx">Editar perfil</a>
                    </div>
                </li>
                <li class="nav-item" style="margin-left:30em;">
                    <a href="frmLogin.aspx" class="nav-link">Cerrar sesión</a>
                </li>
            </ul>
        </nav>
        <div class="container">
            <div class="card">
                <div class="card-header text-center">
                    <h5>Seleccionar Grupo</h5>
                </div>
                <div class="card-body">
                    <asp:Panel runat="server" ID="pnlMensaje" Visible="false">
                        <div class="alert alert-dismissible fade show" role="alert">
                            <asp:Label ID="lblMensaje" Font-Size="13" runat="server" CssClass="text-center"></asp:Label>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </asp:Panel>
                    <br />
                    <asp:Label Text="Grupos" CssClass="form-group" runat="server" />
                    <br />
                    <br />
                    <asp:DropDownList runat="server" ID="ddlgrupo" CssClass="form-control" ></asp:DropDownList>
                    <br />
                    <asp:DropDownList runat="server" ID="ddlestudiante" CssClass="form-control"  ></asp:DropDownList>
                    <br />
                    <asp:Button Text="Registrar" ID="btnRegistrar" runat="server" CssClass="btn btn-block" OnClick="btnRegistrar_Click"/>
                </div>
            </div>
        </div>
    </form>
    <script src="js/jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</body>
</html>
