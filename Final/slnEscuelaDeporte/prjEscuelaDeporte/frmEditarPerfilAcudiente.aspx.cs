﻿using libEscuelaDeporteOP;
using System;
using System.Reflection;

namespace prjEscuelaDeporte
{
    public partial class frmEditarPerfilAcudiente : System.Web.UI.Page
    {
        private static string strNombreApp;
        private void mostrarPanel()
        {
            this.pnlMensaje.Visible = true;
        }
        private void Actualizar()
        {
            clsAcudienteOP acudiente = new clsAcudienteOP(strNombreApp);
            acudiente.Correo = this.txtCorreo.Text.Trim();
            acudiente.Direccion = this.txtDireccion.Text.Trim();
            acudiente.Celular = this.txtCelular.Text.Trim();
            acudiente.Ocupacion = this.txtOcupacion.Text.Trim();
            acudiente.Usuario = this.txtUsuario.Text.Trim();
            acudiente.Contrasena = this.txtContraseña.Text.Trim();

            if (!acudiente.actualizarOP())
            {
                mostrarMsj(acudiente.Error, true);
                acudiente = null;
                return;
            }
            mostrarMsj("Actualizacion Exitosa", false);
            mostrarPanel();
            acudiente = null;
        }
        private void consultar()
        {
            try
            {

                clsAcudienteOP acudiente = new clsAcudienteOP(strNombreApp);

                
                if (!acudiente.consultarOP())
                {
                    mostrarMsj(acudiente.Error, true);
                    acudiente = null;
                    return;
                }
                this.txtCorreo.Text = acudiente.Correo.Trim();
                this.txtDireccion.Text = acudiente.Direccion.Trim();
                this.txtCelular.Text = acudiente.Celular.Trim();
                this.txtOcupacion.Text = acudiente.Ocupacion.Trim();
                this.txtUsuario.Text = acudiente.Usuario.Trim();
                this.txtContraseña.Text = acudiente.Contrasena.Trim();
                
                acudiente = null;
                
            }
            catch (Exception ex)
            {

                mostrarMsj(ex.Message, true);
            }
        }
        private void mostrarMsj(string msj, bool error)
        {
            this.lblMensaje.Text = msj;

            if (msj == string.Empty)
            {
                this.lblMensaje.Visible = false;
                return;
            }
            this.lblMensaje.Visible = true;

            if (error)
            {
                this.lblMensaje.CssClass = "alert alert-dismissible fade show alert-danger h6";
            }
            else
            {
                this.lblMensaje.CssClass = "alert alert-dismissible fade show alert-success h6";
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                strNombreApp = Assembly.GetExecutingAssembly().GetName().Name + ".xml";
                consultar();
            }
        }
        protected void btnEditar_Click(object sender, EventArgs e)
        {
            Actualizar();
            
        }
    }
}