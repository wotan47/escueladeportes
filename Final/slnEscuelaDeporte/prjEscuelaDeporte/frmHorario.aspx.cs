﻿using libEscuelaDeporteOP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace prjEscuelaDeporte
{
    public partial class frmHorario : System.Web.UI.Page
    {
        #region Variables globales
        private static string strNombreApp;
        #endregion

        #region Metodos Privados
        private void mostrarMsj(string msj, bool error)
        {
            this.lblMensaje.Text = msj;

            if (msj == string.Empty)
            {
                this.lblMensaje.Visible = false;
                return;
            }
            this.lblMensaje.Visible = true;

            if (error)
            {
                this.lblMensaje.CssClass = "alert alert-dismissible fade show alert-danger h5";
            }
            else
            {
                this.lblMensaje.CssClass = "alert alert-dismissible fade show alert-success h5";
            }
        }
        private bool validar()
        {
            if (this.txtHoraInicio.Text.Trim() == string.Empty)
            {
                mostrarMsj("Debe ingresar la hora de inicio", true);
                return false;
            }
            if (this.txtHoraFinalizacion.Text.Trim() == string.Empty)
            {
                mostrarMsj("Debe ingresar la hora de finalizacion", true);
                return false;
            }
            if (this.txtDiasEntreno.Text.Trim() == string.Empty)
            {
                mostrarMsj("Debe ingresar los dias de entreno", true);
                return false;
            }
            return true;
        }
        private void mostrarPanel()
        {
            this.pnlMensaje.Visible = true;
        }
        private void Limpiar()
        {
            this.txtHoraInicio.Text = string.Empty;
            this.txtHoraFinalizacion.Text = string.Empty;
            this.txtDiasEntreno.Text = string.Empty;
        }
        private void registrar()
        {
            try
            {
                if (!validar())
                {
                    return;
                }
                clsHorarioOP horario = new clsHorarioOP(strNombreApp);
                horario.HoraInicio = this.txtHoraInicio.Text.Trim();
                horario.HoraFinalizacion = this.txtHoraFinalizacion.Text.Trim();
                horario.DiasEntreno = this.txtDiasEntreno.Text.Trim();

                if (!horario.registrarOP())
                {
                    mostrarMsj(horario.Error, true);
                    horario = null;
                    return;
                }
                mostrarMsj("Registro Exitoso", false);
                mostrarPanel();
                horario = null;
            }
            catch (Exception ex)
            {
                mostrarMsj(ex.Message, true);
            }
        }
        #endregion

        #region Eventos
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                strNombreApp = Assembly.GetExecutingAssembly().GetName().Name + ".xml";
            }
        }
        protected void btnRegistrar_Click(object sender, EventArgs e)
        {
            registrar();
            Limpiar();
        }
        #endregion

    }
}