﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frmEditarPerfilAcudiente.aspx.cs" Inherits="prjEscuelaDeporte.frmEditarPerfilAcudiente" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <link href="img/fond.jpg" rel="icon" />
    <title>Editar perfil acudiente</title>
</head>
<body style="background-image:url(img/fondoPadre.jpg); background-size:cover;">
    <form id="frmEditarPerfilAcudiente" runat="server">
        <nav class="navbar navbar-expand-md fixed-top navbar-dark bg-dark">
            <a href="frmPerfilAcudiente.aspx" class="navbar-brand">Escuela de deportes</a>
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a href="#" class="nav-link">Crear estudiante</a>
                </li>
                <li class="nav-item">
                    <a href="frmSeleccionarGrupoAcudiente.aspx" class="nav-link">Seleccionar grupo</a>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link">Autorizar grupo</a>
                </li>
                <li class="nav-item dropdown ">
                    <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" aria-expanded="true">Perfil</a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item" href="frmEditarPerfilAcudiente.aspx">Editar perfil</a>
                    </div>
                </li>
                <li class="nav-item" style="margin-left:30em;">
                    <a href="frmLogin.aspx" class="nav-link">Cerrar sesión</a>
                </li>
            </ul>
        </nav>
        <br />
        <br />
        <br />
        <br />
        <div class="container">
            <div class="card">
                <div class="card-header text-center bg-warning">
                    <h5>Editar Perfil</h5>
                </div>
                <div class="card-body">
                    <asp:Panel runat="server" ID="pnlMensaje" Visible="false">
                        <div class="alert alert-dismissible fade show" role="alert">
                            <asp:Label ID="lblMensaje" Font-Size="13" runat="server" CssClass="text-center"></asp:Label>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </asp:Panel>
                    <asp:TextBox runat="server"  CssClass="form-control" placeholder="Correo" ID="txtCorreo" required="true" />
                    <br />
                    <asp:TextBox runat="server" CssClass="form-control" placeholder="Direccion" ID="txtDireccion" required="true" />
                    <br />
                    <asp:TextBox runat="server" TextMode="Number" CssClass="form-control" placeholder="Celular" ID="txtCelular" required="true"/>
                    <br />
                    <asp:TextBox runat="server" CssClass="form-control" placeholder="Ocupacion" ID="txtOcupacion" required="true"/>
                    <br />
                    <asp:TextBox runat="server" CssClass="form-control" placeholder="Usuario" ID="txtUsuario" required="true"/>
                    <br />
                    <asp:TextBox runat="server"  CssClass="form-control" placeholder="Contraseña" ID="txtContraseña" required="true"/>
                    <br />
                    <asp:Button Text="Editar" ID="btnEditar" runat="server" CssClass="btn btn-block btn-warning" OnClick="btnEditar_Click"/>
                </div>
            </div>
        </div>
    </form>
    <script src="js/jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</body>
</html>
