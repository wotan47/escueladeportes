﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frmHorario.aspx.cs" Inherits="prjEscuelaDeporte.frmHorario" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <link href="css/padre.css" rel="stylesheet" />
    <link href="img/fond.jpg" rel="icon" />
    <title>Crear horario</title>
</head>
<body>
    <form id="frmHorario" runat="server">
        <nav class="navbar navbar-expand-md fixed-top navbar-dark bg-dark">
            <a href="frmDirector.aspx" class="navbar-brand">Escuela de deportes</a>
            <ul class="navbar-nav ml-auto">
                <li class="nav-item dropdown">
                    <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Directores</a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item" href="#">Registrar Director</a>
                        <a class="dropdown-item" href="#">Ver Director</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Acudientes</a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item" href="#">Registrar Acudiente</a>
                        <a class="dropdown-item" href="#">Ver Acudiente</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Profesores</a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item" href="#">Registrar Profesor</a>
                        <a class="dropdown-item" href="#">Ver Profesor</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Estudiantes</a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item" href="#">Ver Estudiantes</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#">Deportes</a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item" href="#">Crear Deporte</a>
                        <a class="dropdown-item" href="#">Ver Deporte</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#">Escenarios Deportivos</a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item" href="#">Registrar Escenario</a>
                        <a class="dropdown-item" href="#">Ver Escenarios</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Grupos</a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item" href="frmGrupos.aspx">Crear Grupo</a>
                        <a class="dropdown-item" href="frmActualizarGrupo.aspx">Actualizar Grupo</a>
                        <a class="dropdown-item" href="frmVerGrupo.aspx">Ver Grupos</a>
                    </div>
                </li>
                <li class="nav-item dropdown active">
                    <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Horarios</a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item" href="frmHorario.aspx">Crear Horario</a>
                        <a class="dropdown-item" href="verHorario.aspx">Ver Horario</a>
                    </div>
                </li>
                 <li class="nav-item dropdown">
                    <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Perfil</a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item" href="#">Editar perfil</a>
                    </div>
                </li>
                <li class="nav-item" style="margin-left:4em;">
                    <a href="frmLogin.aspx" class="nav-link">Cerrar Sesión</a>
                </li>
            </ul>
        </nav>
        <div class="container">
            <div class="card">
                <div class="card-header text-center bg-danger">
                    <h5>Crear horario</h5>
                </div>
                <div class="card-body">
                    <asp:Panel runat="server" ID="pnlMensaje" Visible="false">
                        <div class="alert alert-dismissible fade show" role="alert">
                            <asp:Label ID="lblMensaje" runat="server" CssClass="text-center"></asp:Label>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </asp:Panel>
                    <asp:TextBox runat="server" CssClass="form-control" TextMode="Time" ID="txtHoraInicio" required="true"/>
                    <br />
                    <asp:TextBox runat="server" CssClass="form-control" TextMode="Time" ID="txtHoraFinalizacion" required="true"/>
                    <br />
                    <asp:TextBox runat="server" CssClass="form-control" placeholder="Dias de entreno ej: Lunes-Viernes" ID="txtDiasEntreno" required="true"/>
                    <br />
                    <asp:Button Text="Registrar" ID="btnRegistrar" runat="server" cssClass="btn btn-block btn-danger" OnClick="btnRegistrar_Click"/>
                </div>
            </div>
        </div>
    </form>
    <script src="js/jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</body>
</html>
