﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Reflection;
using libEscuelaDeporteOP;

namespace prjEscuelaDeporte
{
    public partial class frmSeleccionarGA : System.Web.UI.Page
    {
        #region Variables globales
        private static string strNombreApp;
        #endregion

        #region Metodos Privados
        private void mostrarMsj(string msj, bool error)
        {
            this.lblMensaje.Text = msj;

            if (msj == string.Empty)
            {
                this.lblMensaje.Visible = false;
                return;
            }
            this.lblMensaje.Visible = true;

            if (error)
            {
                this.lblMensaje.CssClass = "alert alert-dismissible fade show alert-danger h6";
            }
            else
            {
                this.lblMensaje.CssClass = "alert alert-dismissible fade show alert-success h6";
            }
        }

        private void mostrarPanel()
        {
            this.pnlMensaje.Visible = true;
        }
        
        private void registrar()
        {
            try
            {
                clsListaClaseOP listaClase = new clsListaClaseOP(strNombreApp);
                listaClase.IdGrupo = int.Parse(this.ddlgrupo.SelectedValue.Trim());
                listaClase.Documento = this.ddlestudiante.SelectedValue.Trim();

                if (!listaClase.registrarOP())
                {
                    mostrarMsj(listaClase.Error, true);
                    listaClase = null;
                    return;
                }
                mostrarMsj("Registro Exitoso", false);
                mostrarPanel();
                listaClase = null;
            }
            catch (Exception ex)
            {
                mostrarMsj(ex.Message, true);
            }
        }
        private void llenarDropDown(DropDownList ddlGen)
        {
            try
            {
                clsLlenarComboOP llenar = new clsLlenarComboOP(strNombreApp);
               
                llenar.DdlGen = ddlGen;
                if (!llenar.llenarDrop())
                {
                    mostrarMsj(llenar.Error, true);
                    llenar = null;
                    return;
                }
            }
            catch (Exception ex)
            {
                mostrarMsj(ex.Message, true);
            }
        }
        #endregion

        #region Eventos
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                strNombreApp = Assembly.GetExecutingAssembly().GetName().Name + ".xml";
                llenarDropDown(ddlgrupo);
                llenarDropDown(ddlestudiante);
                
            }
        }
        
  
        protected void btnRegistrar_Click(object sender, EventArgs e)
        {
            registrar();
        }
        #endregion
    }
}