﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Reflection;
using libEscuelaDeporteOP;
using System.Data.SqlClient;

namespace prjEscuelaDeporte
{
    public partial class frmVerGrupo : System.Web.UI.Page
    {
        #region Variables globales
        private static string strNombreApp;
        #endregion

        #region MetodosPrivados
        private void mostrarMsj(string msj, bool error)
        {
            this.lblMensaje.Text = msj;

            if (msj == string.Empty)
            {
                this.lblMensaje.Visible = false;
                return;
            }
            this.lblMensaje.Visible = true;

            if (error)
            {
                this.lblMensaje.CssClass = "alert alert-dismissible fade show alert-danger h5";
            }
            else
            {
                this.lblMensaje.CssClass = "alert alert-dismissible fade show alert-success h5";
            }
        }
        private void MostrarDatos()
        {
            try
            {
                clsGrupoOP grupo = new clsGrupoOP(strNombreApp);
                if (!grupo.consultarOP(gvGrupo))
                {
                    mostrarMsj(grupo.Error, true);
                    grupo = null;
                    return;
                }
                grupo = null;
            }
            catch (Exception ex)
            {
                mostrarMsj(ex.Message, true);
            }
        }
        private void eliminarDatos(GridViewDeleteEventArgs e)
        {
            try
            {
                clsGrupoOP Grupo = new clsGrupoOP(strNombreApp);
                Grupo.IdGrupo = (int)gvGrupo.DataKeys[e.RowIndex].Values[0];
                if (!Grupo.borrarOP())
                {
                    mostrarMsj(Grupo.Error, true);
                    return;
                }
            }
            catch (Exception ex)
            {
                mostrarMsj(ex.Message, true);
            }
        }
        #endregion

        #region Evento
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                strNombreApp = Assembly.GetExecutingAssembly().GetName().Name + ".xml";
                MostrarDatos();
            }
        }
        protected void gvGrupo_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            eliminarDatos(e);
            if (IsPostBack)
            {
                Server.Transfer("frmVerGrupo.aspx");
            }
        }
        #endregion


    }
}